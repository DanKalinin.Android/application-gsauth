package application.gsauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import application.gsauth.databinding.GsaHomeActivityBinding;
import library.androidx.appcompat.app.LAAAAppCompatActivity;
import library.gsauth2.Gsa2Account;
import library.gsauth2.Gsa2DomainFull;
import library.gsauth2.Gsa2Exception;
import library.gsauth2.Gsa2Token;

public class GsaHomeActivity extends LAAAAppCompatActivity implements View.OnClickListener {
    public static String DOMAIN_ID = "domainId";

    protected GsaHomeActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = GsaHomeActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonEdit.setOnClickListener(this);
        binding.buttonDelete.setOnClickListener(this);
        binding.buttonExit.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        reload();
    }

    @Override
    public void onClick(View sender) {
        try {
            if (sender.equals(binding.buttonEdit)) {
                String domainId = getIntent().getStringExtra(DOMAIN_ID);
                gsaHomeEditActivity(domainId);
            } else if (sender.equals(binding.buttonDelete)) {
                String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
                String domainId = getIntent().getStringExtra(DOMAIN_ID);

                Gsa2Token token = ((GsaApplication)getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

                ((GsaApplication)getApplication()).client.domainDelete(domainId, token.access, (exception) -> {
                    if (exception == null) {
                        finish();
                    } else {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (sender.equals(binding.buttonExit)) {
                String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
                String domainId = getIntent().getStringExtra(DOMAIN_ID);

                Gsa2Token token = ((GsaApplication)getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

                ((GsaApplication)getApplication()).client.domainExit(domainId, token.access, (exception) -> {
                    if (exception == null) {
                        finish();
                    } else {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void reload() {
        try {
            String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
            String domainId = getIntent().getStringExtra(DOMAIN_ID);

            Gsa2DomainFull domainFull = ((GsaApplication)getApplication()).database.domainFullSelectByAccountAndDomain(accountId, domainId)[0];
            Gsa2Account owner = ((GsaApplication)getApplication()).database.accountSelectById(domainFull.domain.owner)[0];

            binding.textViewName.setText(domainFull.domain.name);
            binding.textViewOwner.setText(owner.name);

            binding.frameLayoutExpiration.setVisibility(View.GONE);
            binding.frameLayoutHintOwner.setVisibility(View.GONE);
            binding.frameLayoutHintGuest.setVisibility(View.GONE);
            binding.buttonEdit.setVisibility(View.GONE);
            binding.buttonDelete.setVisibility(View.GONE);
            binding.buttonExit.setVisibility(View.GONE);

            if (domainFull.domain.owner.equals(accountId)) {
                binding.frameLayoutHintOwner.setVisibility(View.VISIBLE);
                binding.buttonEdit.setVisibility(View.VISIBLE);
                binding.buttonDelete.setVisibility(View.VISIBLE);
            } else {
                binding.frameLayoutExpiration.setVisibility(View.VISIBLE);
                binding.frameLayoutHintGuest.setVisibility(View.VISIBLE);
                binding.buttonExit.setVisibility(View.VISIBLE);

                if (domainFull.accountDomain.expiration == null) {
                    binding.textViewExpiration.setText(R.string.unlimited);
                } else {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy - HH:mm").withZone(ZoneId.systemDefault());
                    String expiration = formatter.format(domainFull.accountDomain.expiration);
                    binding.textViewExpiration.setText(expiration);
                }
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void gsaHomeEditActivity(String domainId) {
        Intent intent = new Intent(this, GsaHomeEditActivity.class);
        intent.putExtra(GsaHomeEditActivity.DOMAIN_ID, domainId);
        startActivity(intent);
    }
}
