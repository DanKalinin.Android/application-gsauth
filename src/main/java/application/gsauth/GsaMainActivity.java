package application.gsauth;

import android.os.Bundle;
import android.view.MenuItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import application.gsauth.databinding.GsaMainActivityBinding;
import library.androidx.appcompat.app.LAAAAppCompatActivity;

public class GsaMainActivity extends LAAAAppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    protected GsaMainActivityBinding binding;

    protected GsaAccountFragment accountFragment;
    protected GsaHomesFragment homesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = GsaMainActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.bottomNavigationView.setOnNavigationItemSelectedListener(this);

        accountFragment = new GsaAccountFragment();
        homesFragment = new GsaHomesFragment();

        reload();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem sender) {
        setTitle(sender.getTitle());

        if (sender.getItemId() == R.id.menuItemAccount) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, accountFragment).commit();
        } else if (sender.getItemId() == R.id.menuItemHomes) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, homesFragment).commit();
        }

        return true;
    }

    protected void reload() {
        binding.bottomNavigationView.setSelectedItemId(R.id.menuItemAccount);
    }
}
