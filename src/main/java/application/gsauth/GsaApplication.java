package application.gsauth;

import android.content.SharedPreferences;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import library.android.app.LAAApplication;
import library.android.os.LAOFileUtils;
import library.gsauth2.Gsa2Client;
import library.gsauth2.Gsa2Database;
import library.gsauth2.Gsa2Exception;

public class GsaApplication extends LAAApplication {
    public static String ACCOUNT_ID = "accountId";

    public Gsa2Database database;
    public Gsa2Client client;
    public SharedPreferences preferences;

    @Override
    public void onCreate() {
        super.onCreate();

        try {
            File file = new File(getFilesDir(), Gsa2Database.FILE);

            if (!file.exists()) {
                try (InputStream inputStream = getAssets().open(Gsa2Database.FILE); FileOutputStream outputStream = openFileOutput(Gsa2Database.FILE, MODE_PRIVATE)) {
                    LAOFileUtils.copy(inputStream, outputStream);
                }
            }

            database = Gsa2Database.database(file.getAbsolutePath());
            client = Gsa2Client.client("https://smart-home-cloud.test.gs-labs.tv", database);
            preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }
}
