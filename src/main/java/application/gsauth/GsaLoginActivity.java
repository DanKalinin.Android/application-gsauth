package application.gsauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import application.gsauth.databinding.GsaLoginActivityBinding;
import library.androidx.appcompat.app.LAAAAppCompatActivity;
import library.gsauth2.Gsa2Identity;

public class GsaLoginActivity extends LAAAAppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    protected GsaLoginActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = GsaLoginActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.editTextPhone.setTag(Gsa2Identity.TYPE_PHONE);
        binding.editTextEmail.setTag(Gsa2Identity.TYPE_EMAIL);

        binding.radioButtonPhone.setTag(Gsa2Identity.TYPE_PHONE);
        binding.radioButtonEmail.setTag(Gsa2Identity.TYPE_EMAIL);

        binding.radioGroup.setOnCheckedChangeListener(this);
        binding.buttonNext.setOnClickListener(this);

        reload();
    }

    @Override
    public void onClick(View sender) {
        RadioButton radioButton = findViewById(binding.radioGroup.getCheckedRadioButtonId());
        EditText editText = binding.getRoot().findViewWithTag(radioButton.getTag());

        if (editText.length() == 0) {
            String text = getString(R.string.enter_the_s, radioButton.getText().toString().toLowerCase());
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
            return;
        }

        Gsa2Identity identity = new Gsa2Identity();
        identity.type = radioButton.getTag().toString();
        identity.value = editText.getText().toString();

        ((GsaApplication)getApplication()).client.identityCheck(identity, (available, exception) -> {
            if (exception == null) {
                if (available) {
                    Toast.makeText(this, R.string.account_not_found, Toast.LENGTH_SHORT).show();
                } else {
                    gsaPasswordActivity(identity);
                }
            } else {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onCheckedChanged(RadioGroup sender, int checkedId) {
        reload();
    }

    protected void reload() {
        RadioButton radioButton = findViewById(binding.radioGroup.getCheckedRadioButtonId());
        EditText editText = binding.linearLayout.findViewWithTag(radioButton.getTag());

        binding.editTextPhone.setVisibility(View.GONE);
        binding.editTextEmail.setVisibility(View.GONE);

        editText.setVisibility(View.VISIBLE);
        editText.requestFocus();
    }

    protected void gsaPasswordActivity(Gsa2Identity identity) {
        Intent intent = new Intent(this, GsaPasswordActivity.class);
        intent.putExtra(GsaPasswordActivity.IDENTITY, identity);
        startActivity(intent);
    }
}
