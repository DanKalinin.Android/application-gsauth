package application.gsauth;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import application.gsauth.databinding.GsaHomesViewHolderBinding;
import library.androidx.recyclerview.widget.LARWRecyclerViewAdapter;
import library.gsauth2.Gsa2Account;
import library.gsauth2.Gsa2DomainFull;
import library.gsauth2.Gsa2Exception;

public class GsaHomesAdapter extends LARWRecyclerViewAdapter<GsaHomesViewHolder> {
    protected Gsa2DomainFull[] domainsFull;

    public GsaHomesAdapter(Gsa2DomainFull[] domainsFull) {
        this.domainsFull = domainsFull;
    }

    @Override
    public GsaHomesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        GsaHomesViewHolderBinding binding = GsaHomesViewHolderBinding.inflate(inflater, parent, false);
        GsaHomesViewHolder ret = new GsaHomesViewHolder(binding);
        return ret;
    }

    @Override
    public void onBindViewHolder(GsaHomesViewHolder holder, int position) {
        try {
            Gsa2DomainFull domainFull = domainsFull[position];
            Gsa2Account owner = ((GsaApplication)holder.binding.getRoot().getContext().getApplicationContext()).database.accountSelectById(domainFull.domain.owner)[0];

            holder.domainId = domainFull.domain.id;

            holder.binding.textViewName.setText(domainFull.domain.name);
            holder.binding.textViewOwner.setText(owner.name);

            holder.binding.cardViewOwner.setVisibility(View.GONE);
            holder.binding.cardViewGuest.setVisibility(View.GONE);

            if (domainFull.domain.owner.equals(domainFull.accountDomain.account)) {
                holder.binding.cardViewOwner.setVisibility(View.VISIBLE);
            } else {
                holder.binding.cardViewGuest.setVisibility(View.VISIBLE);
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return domainsFull.length;
    }
}
