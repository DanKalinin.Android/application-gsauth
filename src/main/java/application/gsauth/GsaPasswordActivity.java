package application.gsauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import application.gsauth.databinding.GsaPasswordActivityBinding;
import library.androidx.appcompat.app.LAAAAppCompatActivity;
import library.gsauth2.Gsa2Credential;
import library.gsauth2.Gsa2Identity;

public class GsaPasswordActivity extends LAAAAppCompatActivity implements View.OnClickListener {
    public static String IDENTITY = "identity";

    protected GsaPasswordActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = GsaPasswordActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonNext.setOnClickListener(this);

        reload();
    }

    @Override
    public void onClick(View sender) {
        if (binding.editTextPassword.length() == 0) {
            Toast.makeText(this, R.string.enter_the_password, Toast.LENGTH_SHORT).show();
            return;
        }

        Gsa2Identity identity = (Gsa2Identity)getIntent().getSerializableExtra(IDENTITY);

        Gsa2Credential credential = new Gsa2Credential();
        credential.type = Gsa2Credential.TYPE_PASSWORD;
        credential.value = binding.editTextPassword.getText().toString();

        ((GsaApplication)getApplication()).client.tokenIssue(identity, credential, (token, exception) -> {
            if (token == null) {
                Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            } else {
                ((GsaApplication)getApplication()).client.accountGet(token.access, (account, _exception) -> {
                    if (account == null) {
                        Toast.makeText(this, _exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        ((GsaApplication)getApplication()).preferences.edit().putString(GsaApplication.ACCOUNT_ID, account.id).apply();
                        gsaMainActivity();
                    }
                });
            }
        });
    }

    protected void reload() {
        binding.editTextPassword.requestFocus();
    }

    protected void gsaMainActivity() {
        Intent intent = new Intent(this, GsaMainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}
