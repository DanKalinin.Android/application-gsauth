package application.gsauth;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import application.gsauth.databinding.GsaHomeEditActivityBinding;
import library.androidx.appcompat.app.LAAAAppCompatActivity;
import library.gsauth2.Gsa2Domain;
import library.gsauth2.Gsa2DomainFull;
import library.gsauth2.Gsa2Exception;
import library.gsauth2.Gsa2Token;

public class GsaHomeEditActivity extends LAAAAppCompatActivity implements View.OnClickListener {
    public static String DOMAIN_ID = "domainId";

    protected GsaHomeEditActivityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = GsaHomeEditActivityBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.buttonCreate.setOnClickListener(this);
        binding.buttonSave.setOnClickListener(this);

        reload();
    }

    @Override
    public void onClick(View sender) {
        try {
            if (sender.equals(binding.buttonCreate)) {
                if (binding.editTextName.length() == 0) {
                    Toast.makeText(this, R.string.enter_the_name, Toast.LENGTH_SHORT).show();
                    return;
                }

                String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);

                Gsa2Token token = ((GsaApplication)getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

                Gsa2Domain domain = new Gsa2Domain();
                domain.name = binding.editTextName.getText().toString();

                ((GsaApplication)getApplication()).client.domainCreate(domain, null, token.access, (domainFull, exception) -> {
                    if (domainFull == null) {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        finish();
                    }
                });
            } else if (sender.equals(binding.buttonSave)) {
                if (binding.editTextName.length() == 0) {
                    Toast.makeText(this, R.string.enter_the_name, Toast.LENGTH_SHORT).show();
                    return;
                }

                String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
                String domainId = getIntent().getStringExtra(DOMAIN_ID);

                Gsa2Token token = ((GsaApplication)getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

                ((GsaApplication)getApplication()).client.domainRename(domainId, binding.editTextName.getText().toString(), token.access, (exception) -> {
                    if (exception == null) {
                        finish();
                    } else {
                        Toast.makeText(this, exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void reload() {
        try {
            binding.buttonCreate.setVisibility(View.GONE);
            binding.buttonSave.setVisibility(View.GONE);

            String domainId = getIntent().getStringExtra(DOMAIN_ID);

            if (domainId == null) {
                setTitle(R.string.new_home);

                binding.buttonCreate.setVisibility(View.VISIBLE);
            } else {
                setTitle(R.string.edit_home);

                String accountId = ((GsaApplication)getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);

                Gsa2DomainFull domainFull = ((GsaApplication)getApplication()).database.domainFullSelectByAccountAndDomain(accountId, domainId)[0];

                binding.editTextName.setText(domainFull.domain.name);
                binding.buttonSave.setVisibility(View.VISIBLE);
            }

            binding.editTextName.requestFocus();
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }
}
