package application.gsauth;

import android.content.Intent;
import android.view.View;
import application.gsauth.databinding.GsaHomesViewHolderBinding;
import library.androidx.recyclerview.widget.LARWRecyclerViewViewHolder;

public class GsaHomesViewHolder extends LARWRecyclerViewViewHolder implements View.OnClickListener {
    protected GsaHomesViewHolderBinding binding;
    protected String domainId;

    public GsaHomesViewHolder(GsaHomesViewHolderBinding binding) {
        super(binding.getRoot());

        this.binding = binding;

        binding.getRoot().setOnClickListener(this);
    }

    @Override
    public void onClick(View sender) {
        gsaHomeActivity(domainId);
    }

    protected void gsaHomeActivity(String domainId) {
        Intent intent = new Intent(binding.getRoot().getContext(), GsaHomeActivity.class);
        intent.putExtra(GsaHomeActivity.DOMAIN_ID, domainId);
        binding.getRoot().getContext().startActivity(intent);
    }
}
