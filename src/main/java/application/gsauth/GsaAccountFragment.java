package application.gsauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import application.gsauth.databinding.GsaAccountFragmentBinding;
import library.androidx.fragment.app.LAFAFragment;
import library.gsauth2.Gsa2Account;
import library.gsauth2.Gsa2Exception;
import library.gsauth2.Gsa2Token;

public class GsaAccountFragment extends LAFAFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    protected GsaAccountFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = GsaAccountFragmentBinding.inflate(inflater, container, false);

        binding.swipeRefreshLayout.setOnRefreshListener(this);
        binding.buttonLogin.setOnClickListener(this);
        binding.buttonLogout.setOnClickListener(this);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        reload();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        binding = null;
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @Override
    public void onClick(View sender) {
        if (sender.equals(binding.buttonLogin)) {
            gsaLoginActivity();
        } else if (sender.equals(binding.buttonLogout)) {
            ((GsaApplication)getActivity().getApplication()).preferences.edit().remove(GsaApplication.ACCOUNT_ID).apply();
            reload();
        }
    }

    protected void reload() {
        try {
            binding.swipeRefreshLayout.setEnabled(false);
            binding.linearLayoutLogin.setVisibility(View.GONE);
            binding.linearLayoutAccount.setVisibility(View.GONE);

            String accountId = ((GsaApplication)getActivity().getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);

            if (accountId == null) {
                binding.linearLayoutLogin.setVisibility(View.VISIBLE);
            } else {
                binding.swipeRefreshLayout.setEnabled(true);
                binding.linearLayoutAccount.setVisibility(View.VISIBLE);

                Gsa2Account account = ((GsaApplication)getActivity().getApplication()).database.accountSelectById(accountId)[0];
                binding.textViewName.setText(account.name);
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void refresh() {
        try {
            String accountId = ((GsaApplication)getActivity().getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
            Gsa2Token token = ((GsaApplication)getActivity().getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

            ((GsaApplication)getActivity().getApplication()).client.accountGet(token.access, (account, exception) -> {
                if (binding == null) return;

                binding.swipeRefreshLayout.setRefreshing(false);

                if (account == null) {
                    Toast.makeText(getActivity(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    reload();
                }
            });
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void gsaLoginActivity() {
        Intent intent = new Intent(getActivity(), GsaLoginActivity.class);
        startActivity(intent);
    }
}
