package application.gsauth;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import application.gsauth.databinding.GsaHomesFragmentBinding;
import library.androidx.fragment.app.LAFAFragment;
import library.gsauth2.Gsa2DomainFull;
import library.gsauth2.Gsa2Exception;
import library.gsauth2.Gsa2Token;

public class GsaHomesFragment extends LAFAFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    protected GsaHomesFragmentBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = GsaHomesFragmentBinding.inflate(inflater, container, false);

        binding.swipeRefreshLayout.setOnRefreshListener(this);
        binding.buttonLogin.setOnClickListener(this);
        binding.buttonCreate.setOnClickListener(this);
        binding.floatingActionButtonCreate.setOnClickListener(this);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        binding.recyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), layoutManager.getOrientation());
        binding.recyclerView.addItemDecoration(itemDecoration);

        return binding.getRoot();
    }

    @Override
    public void onStart() {
        super.onStart();

        reload();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        binding = null;
    }

    @Override
    public void onRefresh() {
        refresh();
    }

    @Override
    public void onClick(View sender) {
        if (sender.equals(binding.buttonLogin)) {
            gsaLoginActivity();
        } else if (sender.equals(binding.buttonCreate)) {
            gsaHomeEditActivity();
        } else if (sender.equals(binding.floatingActionButtonCreate)) {
            gsaHomeEditActivity();
        }
    }

    protected void reload() {
        try {
            binding.swipeRefreshLayout.setEnabled(false);
            binding.linearLayoutLogin.setVisibility(View.GONE);
            binding.linearLayoutEmpty.setVisibility(View.GONE);
            binding.frameLayoutHomes.setVisibility(View.GONE);

            String accountId = ((GsaApplication)getActivity().getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);

            if (accountId == null) {
                binding.linearLayoutLogin.setVisibility(View.VISIBLE);
            } else {
                binding.swipeRefreshLayout.setEnabled(true);

                Gsa2DomainFull[] domainsFull = ((GsaApplication)getActivity().getApplication()).database.domainFullSelectByAccount(accountId, "ORDER BY CAST(domain AS INTEGER) DESC");

                if (domainsFull.length > 0) {
                    binding.frameLayoutHomes.setVisibility(View.VISIBLE);

                    GsaHomesAdapter adapter = new GsaHomesAdapter(domainsFull);
                    binding.recyclerView.setAdapter(adapter);
                } else {
                    binding.linearLayoutEmpty.setVisibility(View.VISIBLE);
                }
            }
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void refresh() {
        try {
            String accountId = ((GsaApplication)getActivity().getApplication()).preferences.getString(GsaApplication.ACCOUNT_ID, null);
            Gsa2Token token = ((GsaApplication)getActivity().getApplication()).database.tokenSelectBySubjectAndId(Gsa2Token.SUBJECT_ACCOUNT, accountId)[0];

            ((GsaApplication)getActivity().getApplication()).client.accountGet(token.access, (account, exception) -> {
                if (binding == null) return;

                binding.swipeRefreshLayout.setRefreshing(false);

                if (account == null) {
                    Toast.makeText(getActivity(), exception.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    reload();
                }
            });
        } catch (Gsa2Exception exception) {
            exception.printStackTrace();
        }
    }

    protected void gsaLoginActivity() {
        Intent intent = new Intent(getActivity(), GsaLoginActivity.class);
        startActivity(intent);
    }

    protected void gsaHomeEditActivity() {
        Intent intent = new Intent(getActivity(), GsaHomeEditActivity.class);
        startActivity(intent);
    }
}
